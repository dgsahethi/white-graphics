/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */
$(window).on('load', function(){
    $("#preloader").delay(1500).fadeOut('slow');
});

$(function(){
    $("#team-members").owlCarousel({
        items:2,
        autoplay:true,
        smartSpeed:700,
        loop:true,
        autoplayHoverPause:true,
        dots:false,
        nav:true,
        navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
        responsive:{
            //0 or up wala width
            0:{
                items:1
            },
            480:{
                items:2
            }
        }
    });
    
    $("#progress-elements").waypoint(function(){
        $('.progress-bar').each(function(){
            $(this).animate({
                width: $(this).attr('aria-valuenow') + "%"
            },800);
        });
        this.destroy();
    },{
        offset:'bottom-in-view'
    }
);
});